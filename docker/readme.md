# Docker

Execute the `build_image.sh` script to automatically build and push the image to the registry.

### Build

```zsh
./docker/build_image.sh
Usage: build_image.sh [:hv:k:g:u:a:]

    -h      print this
    -v      version
    -k      kafkadillo version
    -g      gcc version
    -u      ubuntu version, codename or release
    -a      architecture, arm64v8 or amd64
```

### Run

These are the minimal configuration options for moving data between a local cluster to a remote cluster, possibly deployed on [Confluent Cloud](https://www.confluent.io/confluent-cloud/).

```zsh
docker run -it --rm \
    -e CONSUME_TIMEOUT='100' \
    -e PRODUCE_TIMEOUT='100' \
    -e FLUSH_TIMEOUT='100' \
    -e SOURCE_TOPICS='topic1;topic2' \
    -e CONSUMER_BOOTSTRAP_SERVERS='<ip>:<port>' \
    -e CONSUMER_SECURITY_PROTOCOL='plaintext' \
    -e CONSUMER_GROUP_ID='dev' \
    -e PRODUCER_BOOTSTRAP_SERVERS='<ip>:<port>' \
    -e PRODUCER_SECURITY_PROTOCOL='SASL_SSL' \
    -e PRODUCER_SASL_MECHANISMS='PLAIN' \
    -e PRODUCER_SASL_USERNAME='<user>' \
    -e PRODUCER_SASL_PASSWORD='<password>' \
    -e PRODUCER_COMPRESSION_CODEC='lz4' \
    registry.gitlab.com/aiva.vision/cxx-development/conveyor:<tag>
```
