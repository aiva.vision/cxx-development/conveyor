#!/bin/bash

optstring=":hv:k:g:u:a:"

function usage {
  printf "Usage: $(basename $0) [${optstring}]\n\n"
  printf "    -h      print this\n"
  printf "    -v      version\n"
  printf "    -k      kafkadillo version\n"
  printf "    -g      gcc version\n"
  printf "    -u      ubuntu version, codename or release\n"
  printf "    -a      architecture, arm64v8 or amd64\n"
}

if [[ $# -eq 0 ]]; then
  usage
  exit 0
fi

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      usage
      exit 0
      ;;
    v)
      conveyor_version=${OPTARG}
      ;;
    k)
      kafkadillo_version=${OPTARG}
      ;;
    g)
      gcc_version=${OPTARG}
      ;;
    u)
      ubuntu_version=${OPTARG}
      ;;
    a)
      architecture=${OPTARG}
      ;;
    :)
      echo "$0: Must supply an argument to -${OPTARG}." >&2
      exit 1
      ;;
    ?)
      echo "$0: Invalid option: -${OPTARG}."
      exit 2
      ;;
  esac
done

for variable_name in "kafkadillo_version" "conveyor_version" "gcc_version" "ubuntu_version" "architecture"
do
  if [ -z ${!variable_name} ]; then
    echo "$(basename $0): Setting ${variable_name} is mandatory."
    exit 1
  fi
done


tag="${conveyor_version}-k${kafkadillo_version}-${ubuntu_version}-${architecture}"

echo "Building image with tag ${tag} ..."

DOCKER_BUILDKIT=1 docker build \
  --cpuset-cpus $(nproc) \
  --build-arg kafkadillo_version=${kafkadillo_version} \
  --build-arg gcc_version=${gcc_version} \
  --build-arg ubuntu_version=${ubuntu_version} \
  --build-arg architecture=${architecture} \
  -t registry.gitlab.com/aiva.vision/cxx-development/conveyor:${tag} \
  -f docker/Dockerfile .
docker push registry.gitlab.com/aiva.vision/cxx-development/conveyor:${tag}
