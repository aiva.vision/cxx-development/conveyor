#pragma once

#include <kafkadillo/consumer.hxx>
#include <kafkadillo/producer.hxx>

#include "safe_queue.hxx"

namespace Env_Vars {

extern const char *flush_timeout;
extern const char *consume_timeout;
extern const char *produce_timeout;

} // namespace Env_Vars

using Message_Queue = Safe_Queue<std::unique_ptr<RdKafka::Message>>;
using Shared_Message_Queue = std::shared_ptr<Message_Queue>;

using Shared_Consumer = std::shared_ptr<kdillo::Consumer>;

class Conveyor {
public:
  using Milliseconds = std::chrono::milliseconds;
  /*
     Default properties for the consumer.
     */
  static kdillo::Properties consumer_enforced;
  /*
     Default properties for the producer.
     */
  static kdillo::Properties producer_enforced;
  /*
     Instantiates the conveyor or returns the existing instance.
     */
  static std::unique_ptr<Conveyor> &get_instance();
  /*
     Starts conveying messages from the source cluster to the destination
     cluster.
     */
  void start();
  /*
     Exits from the conveying loop.
     */
  void stop();

private:
  /*
     Unique instance for the Conveyor.
     */
  static std::unique_ptr<Conveyor> instance;
  /*
     Configures the consumer and the producer from the environment.

     May throw: std::runtime_error
     */
  Conveyor();
  /*
     Consumes a message and sends it to the destination topic.
     */
  void convey_message();
  /*
     Flag that can be modified through the stop() method.
     */
  bool should_stop{false};
  /*
     Timeout for consuming messages.
     */
  Milliseconds consume_timeout;
  /*
     Timeout for producing messages.
     */
  Milliseconds produce_timeout;
  /*
     Messages awaiting delivery confirmation.
     */
  Shared_Message_Queue messages;

  Shared_Consumer consumer;
  kdillo::Producer producer;
};
