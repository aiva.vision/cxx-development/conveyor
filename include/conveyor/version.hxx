#pragma once

#include <string>

namespace Conveyor {

std::string get_project_version();

unsigned get_project_version_major();

unsigned get_project_version_minor();

unsigned get_project_version_patch();

unsigned get_project_version_tweak();

} // namespace Conveyor
