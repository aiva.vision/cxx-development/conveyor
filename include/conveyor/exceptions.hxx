#pragma once

#include <string>

class Error : public std::exception {
public:
  Error(const std::string &m);

  virtual ~Error() {}

  const char *what() const noexcept;

private:
  std::string message;
};
