#pragma once

#include <kafkadillo/consumer.hxx>

#include "conveyor/conveyor.hxx"
/*
   Delivery callback that stores the offsets for the consumer if and only if the
   message has been successfully delivered.
   */
class Commit_Offset_Cb : public RdKafka::DeliveryReportCb {
public:
  Commit_Offset_Cb(Shared_Message_Queue &m, Shared_Consumer &c)
      : messages{m}, consumer{c} {}
  /*
     Commit the offset for the successfully delivered messages.
     */
  void dr_cb(RdKafka::Message &m) override;

private:
  Shared_Message_Queue messages;
  Shared_Consumer consumer;
};
