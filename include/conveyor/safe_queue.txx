#pragma once

#ifndef SAFE_QUEUE
#include "conveyor/safe_queue.hxx"
#endif // SAFE_QUEUE
#include "conveyor/exceptions.hxx"

template <typename T> Safe_Queue<T>::Safe_Queue(Safe_Queue &&q) {

  std::unique_lock<std::mutex> lck{q.mtx};
  elements = std::move(q.elements);
  should_wait = std::move(q.should_wait);
}

template <typename Output>
Safe_Queue<Output> &Safe_Queue<Output>::operator=(Safe_Queue &&q) {

  if (this != &q) {

    std::unique_lock<std::mutex> lhs_lck{mtx, std::defer_lock};
    std::unique_lock<std::mutex> rhs_lck{q.mtx, std::defer_lock};
    std::lock(lhs_lck, rhs_lck);

    elements = std::move(q.elements);
    should_wait = std::move(q.should_wait);
  }
  return *this;
}

template <typename T> void Safe_Queue<T>::push(T &&b) {

  std::unique_lock<std::mutex> lock{mtx};
  elements.push(std::move(b));
  condition.notify_one();
}

template <typename T> void Safe_Queue<T>::push(const T &b) {

  std::unique_lock<std::mutex> lock{mtx};
  elements.push(b);
  condition.notify_one();
}

template <typename T> T Safe_Queue<T>::pop() {

  std::unique_lock<std::mutex> lock{mtx};
  /*
     Block until an element is pushed again.
     */
  while (elements.empty() && should_wait) {
    condition.wait(lock,
                   [this]() { return !elements.empty() || !should_wait; });
  }
  if (elements.empty()) {
    throw Error{"Can not pop from an empty queue"};
  }
  /*
     Avoid copying.
     */
  T elem{std::move(elements.front())};
  elements.pop();

  return elem;
}

template <typename T>
std::optional<T> Safe_Queue<T>::try_pop(bool should_block) {

  std::unique_lock<std::mutex> lock{mtx};
  /*
     Block only if asked to.
     */
  while (should_block && elements.empty() && should_wait) {
    condition.wait(lock,
                   [this]() { return !elements.empty() || !should_wait; });
  }
  if (elements.empty()) {
    return {};
  }
  /*
     Avoid copying.
     */
  T elem{std::move(elements.front())};
  elements.pop();

  return elem;
}

template <typename T> void Safe_Queue<T>::clear() {

  std::unique_lock<std::mutex> lock{mtx};
  elements = std::queue<T>{};
}

template <typename T> void Safe_Queue<T>::disable_wait() noexcept {
  should_wait = false;
  condition.notify_all();
}
