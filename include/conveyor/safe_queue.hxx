#pragma once

#define SAFE_QUEUE

#include <condition_variable>
#include <mutex>
#include <optional>
#include <queue>

/*
   Thread safe queue.
   */
template <typename T> class Safe_Queue {
public:
  Safe_Queue() = default;

  Safe_Queue(Safe_Queue &&);

  Safe_Queue(const Safe_Queue &) = delete;

  Safe_Queue &operator=(Safe_Queue &&);

  Safe_Queue &operator=(const Safe_Queue &) = delete;

  ~Safe_Queue() = default;

  bool empty() const noexcept { return elements.empty(); }

  size_t size() const noexcept { return elements.size(); }
  /*
     Inserts an element at the end.
     */
  void push(T &&b);
  /*
     Inserts an element at the end.
     */
  void push(const T &b);
  /*
     Removes the first element and returns it.
     Blocks until an element is available.
     */
  T pop();
  /*
     Removes the first element and returns it.
     */
  std::optional<T> try_pop(bool block = false);

  void clear();
  /*
     Disable waiting for new elements in pop() operations.
     */
  void disable_wait() noexcept;

private:
  std::mutex mtx;
  std::condition_variable condition;
  std::queue<T> elements;
  bool should_wait{true};
};

#include "conveyor/safe_queue.txx"
