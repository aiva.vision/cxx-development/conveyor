#include <spdlog/spdlog.h>

#include "conveyor/commit_offset_cb.hxx"

void Commit_Offset_Cb::dr_cb(RdKafka::Message &m) {

  if (m.err()) {
    throw Error{"Failed to deliver message: " + m.errstr()};
  } else {
    auto consumed_message = messages->try_pop(true);

    if (!consumed_message) {
      throw Error{"The message queue is empty"};
    }
    (*consumer)->commitAsync(consumed_message->get());

    spdlog::debug("Delivered message to {}: '{}'\n", m.topic_name(),
                  static_cast<char *>(consumed_message.value()->payload()));
  }
}
