#include "conveyor/exceptions.hxx"

Error::Error(const std::string &m) : message{m} {}

const char *Error::what() const noexcept { return message.c_str(); }
