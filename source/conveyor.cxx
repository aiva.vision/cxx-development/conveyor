#include <kafkadillo/env.hxx>
#include <spdlog/spdlog.h>

#include "conveyor/commit_offset_cb.hxx"
#include "conveyor/conveyor.hxx"

const char *Env_Vars::consume_timeout = "CONSUME_TIMEOUT";
const char *Env_Vars::produce_timeout = "PRODUCE_TIMEOUT";
const char *Env_Vars::flush_timeout = "FLUSH_TIMEOUT";

kdillo::Properties Conveyor::consumer_enforced{
    {"enable.auto.commit", "false"},
    {"enable.auto.offset.store", "false"},
};

kdillo::Properties Conveyor::producer_enforced{
    {"message.timeout.ms", "0"},
    {"enable.idempotence", "true"},
};

std::unique_ptr<Conveyor> &Conveyor::get_instance() {
  if (!instance) {
    instance.reset(new Conveyor);
  }
  return instance;
}

std::unique_ptr<Conveyor> Conveyor::instance{};

static Shared_Consumer create_consumer() {

  auto topics = kdillo::env::vector(kdillo::Consumer::default_topics_env_var);

  auto props =
      kdillo::env::kafka_properties(kdillo::Consumer::default_env_prefix);

  for (auto &[key, val] : Conveyor::consumer_enforced) {
    props.insert_or_assign(key, val);
  }
  return std::make_shared<kdillo::Consumer>(topics, props);
}

static kdillo::Producer create_producer(Shared_Message_Queue &m,
                                        Shared_Consumer &c) {

  auto props =
      kdillo::env::kafka_properties(kdillo::Producer::default_env_prefix);

  for (auto &[key, val] : Conveyor::producer_enforced) {
    props.insert_or_assign(key, val);
  }
  return kdillo::Producer{props, std::make_unique<Commit_Offset_Cb>(m, c)};
}

Conveyor::Conveyor()
    : messages{std::make_shared<Message_Queue>()}, consumer{create_consumer()},
      producer{create_producer(messages, consumer)} {

  consume_timeout =
      Milliseconds{kdillo::env::ul_var(Env_Vars::consume_timeout)};
  produce_timeout =
      Milliseconds{kdillo::env::ul_var(Env_Vars::produce_timeout)};

  auto flush_timeout =
      Milliseconds{kdillo::env::ul_var(Env_Vars::flush_timeout)};

  producer.set_flush_timeout(flush_timeout);
}

void Conveyor::convey_message() {

  auto message = consumer->consume_rdk(consume_timeout);

  if (!message || !message->payload()) {
    return;
  }
  producer.produce(message.get(), message->topic_name(), produce_timeout);
  messages->push(std::move(message));
}

void Conveyor::start() {

  spdlog::info("Started conveying messages.");

  while (!should_stop) {
    convey_message();
  }
}

void Conveyor::stop() {
  should_stop = true;
  messages->disable_wait();
}
