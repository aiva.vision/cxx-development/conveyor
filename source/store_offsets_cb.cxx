#include <spdlog/spdlog.h>

#include "conveyor/store_offsets_cb.hxx"

void Store_Offsets_Cb::dr_cb(RdKafka::Message &message) {
  if (message.err()) {
    spdlog::error("Failed to deliver message: {}", message.errstr());
  } else {
    store_offsets(message);

    spdlog::debug(
        "Delivered message with partition {} and offset {} to {}: '{}'\n",
        message.partition(), message.offset(), message.topic_name(),
        static_cast<char *>(message.payload()));
  }
}

void Store_Offsets_Cb::store_offsets(RdKafka::Message &message) {

  std::unique_ptr<RdKafka::TopicPartition> tp{RdKafka::TopicPartition::create(
      message.topic_name(), message.partition(), message.offset() + 1)};

  std::vector<RdKafka::TopicPartition *> offsets{tp.get()};

  (*consumer)->offsets_store(offsets);
}
