#define DOCTEST_CONFIG_IMPLEMENT
#include <doctest/doctest.h>

#include <spdlog/cfg/env.h>
#include <spdlog/spdlog.h>

#include "conveyor/conveyor.hxx"

void stop(int signal_number) {

  spdlog::info("Caught signal with ID {}, stopping Conveyor...", signal_number);
  Conveyor::get_instance()->stop();
}

int main(int argc, char *argv[]) {

  spdlog::cfg::load_env_levels();
  /*
     Register signal handlers to exit gracefully.
     */
  signal(SIGINT, stop);
  signal(SIGTERM, stop);

  doctest::Context context;
  context.setOption("exit", false);
  context.applyCommandLine(argc, argv);

  int res = context.run();
  if (context.shouldExit()) {
    return res; // for the query flags
  }
  /*
     Start conveying messages between clusters.
     */
  Conveyor::get_instance()->start();

  spdlog::info("Exited gracefully.");
  return 0;
}
