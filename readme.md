# Conveyor

Replicates data from one cluster to the other. Just like the [Confluent Replicator](https://docs.confluent.io/platform/current/multi-dc-deployments/replicator/index.html), but lighter and configured solely through environment variables. The message offset is committed for each message in the producer's delivery callback, ensuring no data loss even in case of network and power failures.

Visit the Docker [guide](docker) for building Docker images.

### Property conversion rules

1. Pick a property from the [documentation](https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md).

2. Convert to upper-case.

3. Prefix with `CONSUMER_` or `PRODUCER_`.

4. Replace a period (`.`) with a single underscore (`_`).

Examples:

- `bootstrap.servers` for producer -> `PRODUCER_BOOTSTRAP_SERVERS`
- `compression.codec` for consumer -> `CONSUMER_COMPRESSION_CODEC`

### Conveyor properties

The following consumer properties will be set and overwritten if already set:

- `enable.auto.commit`: `false`, the consumer offsets will be committed for every message in the delivery callback registered for the producer
- `enable.auto.offset.store`: `false`, the consumer offsets are committed directly

The following producer properties will be set and overwritten if already set:

- `message.timeout.ms`: `0`, the producer will try to send a message indefinitely
- `enable.idempotence`: `true`, guarantees the message order, crucial for the offset committing delivery callback

Mandatory Conveyor properties:

- `SOURCE_TOPICS`: list of `;` separated topic names from the source cluster
- `CONSUME_TIMEOUT`: in ms
- `PRODUCE_TIMEOUT`: in ms
- `FLUSH_TIMEOUT`: in ms

The consumed messages will be produced to the homonymous topics.

### Dependencies

- [CMake](https://cmake.org/install/)
- [OpenSSL](https://www.openssl.org/)
- [Doctest](https://github.com/onqtam/doctest)
- [Trompeloeil](https://github.com/rollbear/trompeloeil)
- [kafkadillo](https://gitlab.com/aiva.vision/cxx-development/kafkadillo)

1. Clone the project.

```zsh
git clone git@gitlab.com:aiva.vision/conveyor.git
```

2. Build the project and install.

```zsh
mkdir -p conveyor/release

cmake -S conveyor -B conveyor/release \
  -D CMAKE_BUILD_TYPE=Debug \
  -D build_tests=ON

cmake --build conveyor/release -j

cmake --install conveyor/release
```

### Usage

1. Follow the instructions from the [readme](../readme.md) to convert the properties in the right format.

2. Export the environment variables.

```zsh
# Minimal config options to relocate data from a local cluster to a remote one.
export SOURCE_TOPICS='topic1;topic2'
export CONSUMER_BOOTSTRAP_SERVERS='<ip>:<port>'
export CONSUMER_SECURITY_PROTOCOL='plaintext'
export CONSUMER_GROUP_ID='dev'
export PRODUCER_BOOTSTRAP_SERVERS='<ip>:<port>'
export PRODUCER_SECURITY_PROTOCOL='SASL_SSL'
export PRODUCER_SASL_MECHANISMS='PLAIN'
export PRODUCER_SASL_USERNAME='<user>'
export PRODUCER_SASL_PASSWORD='<password>'
export PRODUCER_COMPRESSION_CODEC='lz4'
```

3. Run.

For debugging `SPDLOG_LEVEL` can be set to `debug` and the conveyed messages will be logged to the standard output.

```zsh
conveyor
```
